"use strict";
const { useState,useEffect } = React;


function Image({curItem,idx,option}){
  //If track gives you a valid response,then it will give you the image of the album
  if (option == "track") {
    return (
      idx && curItem && curItem.tracks ?
      <img src={curItem.tracks.items[idx].album.images[0].url}></img>:
      null
    );
  }
  //If album gives you a valid response,then it will give you the image of the album`
  if (option =="album"){
    return (
      curItem && curItem.albums && curItem.albums.items[0] && curItem.albums.items[0].images[0] && curItem.albums.items[0].images[0].url ?
      <img src={curItem.albums.items[0].images[0].url}></img>:
      null
    );
  }
  //If artist gives you a valid response,then it will give you the image of the artist
  if(option =="artist"){
    return(
      curItem && curItem.artists && curItem.artists.items[0] && curItem.artists.items[0].images[0].url?
      <img src={curItem.artists.items[0].images[0].url}></img>:
      null
    );
  }
}

function Audio({curItem,idx,option}){
  //If track gives you a valid response,then it will give you the audio of the track
  if (option == "track"){
    return (
      idx && curItem && curItem.tracks ?
        <div style={{"color":"white"}}> Song Preview: 
          <audio key={curItem.tracks.items[idx].preview_url} controls autoplay>
            <source src={curItem.tracks.items[idx].preview_url}/>
          </audio>
        </div>:
      null
    );
  //If album gives you a valid response,then it will give you the audio of the album
  } else if (option == "album"){
    if(curItem && curItem.albums && curItem.albums.items.length == 0){
      return null
    }
    return(
      curItem  ?
      <a href={curItem.albums.items[0].external_urls.spotify}>Link to album:  {curItem.albums.items[0].name}</a>:
      null
    );
  //If artist gives you a valid response,then it will give you the audio of the artist
  } else if (option =="artist"){
    if(curItem && curItem.artists && curItem.artists.items.length == 0){
      return null
    }
    return(
      curItem ?
      <a href={curItem.artists.items[0].external_urls.spotify}>Link to artist:  {curItem.artists.items[0].name}</a>:
      null
    );
  }
}

function RetrieveLi({curItem,option,index}) {
  //If track gives you a valid response,then it will give you the list of tracks
  if(option == "track"){
    if(curItem && curItem.tracks.items.length == 0){
      return <li>No item found</li>
    }else {
      return(
        curItem && curItem.tracks.items.map((itenz,idx) => {
          return (<li className={index && index == idx ? "selected" : "unselected"} indexes={idx}>{itenz.name}</li>);
        })
      );
    }
  //If album gives you a valid response,then it will give you the list of albums
  } else if (option =="album"){
    if(curItem && curItem.albums && curItem.albums.items.length == 0){
      return <li>No item found</li>
    } else{
      return(
        curItem  ?
        <li>{curItem.albums.items[0].name}</li>:
        null
      );
    }
  //If artist gives you a valid response,then it will give you the list of artists
  } else if (option == "artist"){
    if(curItem && curItem.artists.items.length == 0){
      return <li>No item found</li>
    } else{
      return(
        curItem ?
        <li>{curItem.artists.items[0].name}</li>:
        null
      );
    }
  }
}

function MainBodyPart({curItem, idx, option}) {
  return <main>
    <Image curItem={curItem} idx={idx} option={option} />
    <div class="artist-info">
      <Audio curItem={curItem} idx={idx} option={option} />
    </div>
  </main>;
}

function Sidebar({setOption, setCurItem, setcurrSearched, currSearched, option, token, setIdx,index, curItem,url,tracks,setTracks}) {
  return <header>
    <form onSubmit={(e) => {
          searchFetch(e, currSearched, option, token, setCurItem, setIdx);
        } }>
      <label style={{"font-weight": "bold"}}>Spotify</label>
      <div id="searchBox">
        <label style={{ "font-size": "28px" }}>Choose type of </label>
        <select id="selecting" onChange={(e) => {
          e.preventDefault();
          setOption(e.target.value);
          setCurItem(null);
        } }>
          <option value="track">Tracks</option>
          <option value="album">Albums</option>
          <option value="artist">Artists</option>
        </select>
        <div id="searchDiv">
          <div style={{"font-size":"20px"}}>↓Enter your search query!↓</div>
          <input type="search" id="searchType" onChange={(e) => { setcurrSearched(e.target.value); } }></input>
          <button type="submit" id="searchButton">Search</button>
        </div>
      </div>
      <ul onClick={(e) => {
        e.preventDefault();
        let index = e.target.getAttribute("indexes");
        setTracks([...tracks,curItem.tracks.items[index].uri]);
        setIdx(index);
      } }>
        <RetrieveLi curItem={curItem} option={option} index={index}/>
      </ul>
    </form>
    <Login url={url}/>
    <button type="click" class="login-add" onClick={() => {createPlaylist(tracks,token,setTracks)}}>Add To Spotify</button>
  </header>;
}
function searchFetch(e, currSearched, option, token, setCurItem, setIdx) {
  e.preventDefault();
  fetch(`https://api.spotify.com/v1/search?q=${currSearched}&type=${option}&limit=10`, {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }).then((r) => r.json()).then((data) => {
    if (data.error) {
      setCurItem(null);
      if (data.error.status == 401) {
        alert("Please login to spotify to continue using this app");
      } else if (data.error.status == 400) {
        alert("Please enter something valid in the search box before pressing search <3");
      }
    } else {
      console.log(data);
      setCurItem(data);
      setIdx(null);
    }
  }).catch((err) => {
    console.log(err);
  });
}

//Anything above this line is mostly within the scope of the assignment
//The part below is for the innerworking of the logining aspect of spotify, getting the access token and creating the playlist
//Login to spotify
function Login({url}) {
  return <button style={{"flex-direction":"row-reverse"}} class="login-add" onClick={(e) => {
    e.preventDefault();
    window.location.href = url;
  } }>Login to Spotify</button>;
}
function getAccessToken(clientId, setToken,client_secret,redirectUri) {
  if(!(window.location.href).split("?code=")[1]){ //Checking if there is anything after the / at the end
  return;
  }
  if(!localStorage.getItem("token")){
    let code = (window.location.href.split("?code=")[1]);
    //https://developer.spotify.com/documentation/web-api/tutorials/code-flow Took this from this link,  header and body
    fetch(`https://accounts.spotify.com/api/token`,{
      body : new URLSearchParams({
          code: code,
          redirect_uri: redirectUri,
          grant_type: 'authorization_code'
        }),
        method: 'POST',
        headers:{
          'Content-type': 'application/x-www-form-urlencoded',
          //https://developer.mozilla.org/en-US/docs/Web/API/btoa Used this as a replacement of  this (new Buffer.from(client_id + ':' + client_secret).toString('base64'))
          //As Buffer is a node.js thing and online I found this as a replacement when I searched a replacement for Buffer on google
          'Authorization': 'Basic ' + btoa(clientId + ':' + client_secret)
        },
        json: true
      }
    ).then((r)=>r.json()).then((data) => {
      localStorage.setItem("token",data.access_token);
      localStorage.setItem("refresh_token",data.refresh_token);
      localStorage.setItem("expires",new Date().getTime() + (data.expires_in * 1000));
      setToken(data.access_token);
    }).catch((err) => {
      console.log(err);
    });
    return;
  } else{
    if (new Date().getTime() > localStorage.getItem("expires")) {
      fetch(`https://accounts.spotify.com/api/token`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + btoa(clientId + ':' + client_secret)
          },
          body: new URLSearchParams({
            grant_type: 'refresh_token',
            refresh_token: localStorage.getItem("refresh_token"),
            client_id: clientId
          }),
        }).then((r) => r.json()).then((data) => {
          localStorage.setItem("token", data.access_token);
          localStorage.setItem("refresh_token", data.refresh_token);
          localStorage.setItem("expires",new Date().getTime() + (data.expires_in   * 1000));
          setToken(data.access_token);
        });
  
    }
    setToken(localStorage.getItem("token"));
    return;
  }
}
function createPlaylist(tracks,token,setTracks){
  //https://stackoverflow.com/questions/40981040/using-a-fetch-inside-another-fetch-in-javascript
  //https://www.pluralsight.com/guides/handling-nested-http-requests-using-the-fetch-api
  //Used these to understand nested fetches Basically fetches are promise, so putting them nested makes more sense otherwise it shouldnt work
  ////https://developer.spotify.com/documentation/web-api/reference/create-playlist
  //Used this to understand how to post to spotify
  if(!token || tracks.length <= 0){
    alert("Please search something up before trying to add a playlist");
    return;
  }
  fetch(`https://api.spotify.com/v1/me`,{
    method: 'GET',
    headers: {
    'Authorization': `Bearer ${token}`
    }
  })
  .then((r) => r.json())
  .then((data) => {
    const uID = data.id;
    
    return fetch(`https://api.spotify.com/v1/users/${uID}/playlists`,{
        headers: {
            'Authorization': `Bearer ${token}`
        },
        method: 'POST',
        body: JSON.stringify({
            "name": "Koala in Paris PLaylist (Searched)",
            "description": "Koalas have big noses",
            "public": false
        })
    })
    .then((r) => r.json())
    .then((data) => {
        return fetch(`https://api.spotify.com/v1/playlists/${data.id}/tracks`,{
            method: 'POST',
            headers: {
              'Authorization': `Bearer ${localStorage.getItem('token')}`
          },body: JSON.stringify({
            uris: tracks
          })
        })
        .then((r) => r.json());
    });
  });
  setTracks([]);
}

function App(){
  const [currSearched,setcurrSearched] = useState("");
  const [curItem,setCurItem] = useState("");
  const [idx,setIdx] = useState(null);
  const [option,setOption] = useState("track");

  const [tracks,setTracks] = useState([]);

  const [token,setToken] = useState("");
  const clientId = 'e4d5fcd143e540a582b08ce60e75b96c';
  const client_secret = '92d65cb9b5e84a22b75794f9a4f56114';
  const redirectUri = (window.location.href).split("?code=")[0]; //changed it to this for flexibility (being able to use it through live server and sonic)
  const scopes = 'playlist-modify-private playlist-modify-public'; // switch the type to code later
  const url = `https://accounts.spotify.com/authorize?response_type=code&client_id=${clientId}&scope=${encodeURIComponent(scopes)}&redirect_uri=${encodeURIComponent(redirectUri)}`;
  useEffect(() => {
    //Had an issue with the token not being set because of how useEffect worked, so I looked up how to make the program wait before continuing, and found these magical keywords
    //https://www.altcademy.com/blog/how-to-wait-for-a-function-to-finish-in-javascript/#:~:text=The%20Modern%20Way%3A%20Async%2FAwait,-There's%20a%20more&text=The%20async%20keyword%20tells%20JavaScript,result%20%3D%20await%20myPromise%3B%20console.
    async function getTokensr(){
     await getAccessToken(clientId, setToken,client_secret,redirectUri);
    }
    getTokensr();
  }, []);
  return(
    <div class="wrapper">
      <Sidebar setOption={setOption} setCurItem={setCurItem} setcurrSearched={setcurrSearched} currSearched={currSearched} option={option} token={token} index={idx} setIdx={setIdx} setToken={setToken} curItem={curItem} url={url} tracks={tracks} setTracks={setTracks}/>
      <MainBodyPart curItem={curItem} idx={idx} option={option}/>
    </div>
  );
}
ReactDOM.render(<App/>,document.querySelector('#root'));
//https://developer.spotify.com/ <-- inspired by this to make the create playlist
//https://developer.spotify.com/documentation/web-api
//https://developer.spotify.com/documentation/web-api/concepts/scopes
//https://developer.spotify.com/documentation/web-playback-sdk/howtos/web-app-player
//https://developer.spotify.com/documentation/web-api/tutorials/code-flow
//https://developer.spotify.com/documentation/web-api/concepts/authorization
//https://developer.spotify.com/documentation/web-api/tutorials/refreshing-tokens
//https://developer.spotify.com/documentation/web-api/reference/create-playlist
//https://discuss.codecademy.com/t/react-jamming-project-create-playlist-403-insufficient-client-scope/692226 <-- helped me debug
//https://stackoverflow.com/questions/40981040/using-a-fetch-inside-another-fetch-in-javascript
//https://www.pluralsight.com/guides/handling-nested-http-requests-using-the-fetch-api
//https://dev.to/dom_the_dev/how-to-use-the-spotify-api-in-your-react-js-app-50pn
//https://developer.mozilla.org/en-US/docs/Web/API/btoa
//https://www.altcademy.com/blog/how-to-wait-for-a-function-to-finish-in-javascript/#:~:text=The%20Modern%20Way%3A%20Async%2FAwait,-There's%20a%20more&text=The%20async%20keyword%20tells%20JavaScript,result%20%3D%20await%20myPromise%3B%20console.
