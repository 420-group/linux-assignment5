## Author:
Vijay Patel 440 

## Assignment:
Deploy 320-assignment 6 to a container on docker 

## Application: 
Spotify app which lets you browse through artists, songs, albums, and add the selected ones to a playlist on spotify

## Instructions for Docker:

docker build -t {directory}

docker run -dit --name {name} -p 8080:80 app
