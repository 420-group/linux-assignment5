# For more info, check this https://hub.docker.com/_/httpd
# docker build -t <tag> . 
# docker run todo
FROM httpd
# Label
LABEL maintainer="VijayPatel" update="2024/05/13"

# set the Document root of the container
WORKDIR /usr/local/apache2/htdocs/

EXPOSE 80

# Copy the app to the Document Root
COPY ./app/ .
